const canvas=document.getElementById('draw');
const ctx=canvas.getContext('2d');
const width=canvas.getAttribute("width");//canvas寬
const height=canvas.getAttribute("height");//canvas高

let is_mouse_active=false;//mouse是否觸發

let start_x=0,start_y=0;//起始滑鼠座標
let end_x=0,end_y=0;//終點滑鼠座標
let url="";

//undo redo使用
let step = -1;
let userhistory = [];

//輸入文字使用
let textbox=document.getElementById("textbox");
let textflag=false;
let textcontent="";

//工具列 畫筆:0 橡皮擦:1 文字:2 長方形:3 圓形:4 三角形:5
let tool=[get_pencil=false,get_eraser=false,get_text=false,get_rectangle=false,get_circle=false,get_triangle=false];
let tool_id=['pencil','eraser','text','rectangle','circle','triangle'];

function loadImage(){
    var img = new Image();
    img.src=userhistory[step];
    ctx.drawImage(img,0,0,width,height);
}

//upload
function drawToCanvas(imgData){
    var img = new Image;
    img.src = imgData;
    img.onload = function(){//必須onload之後再畫
    ctx.drawImage(img,0,0,width,height);
    
    //undo redo
    url = canvas.toDataURL();
    step++;
    if(step<userhistory.length)userhistory.length=step;
    userhistory.push(url);
    }

}

function readFile(){
    var file = this.files[0];//獲取input輸入的圖片
    /*if(!/image\/\w /.test(file.type)){
    alert("請確保檔案為影象型別");
    return false;
    }//判斷是否圖片，在移動端由於瀏覽器對呼叫file型別處理不同，雖然加了accept = 'image/*'，但是還要再次判斷*/
    var reader = new FileReader();
    reader.readAsDataURL(file);//轉化成base64資料型別
    reader.onload = function(e){
    drawToCanvas(this.result);
    }
}

function upload()
{
    var inputobj = document.createElement('input');
    inputobj.addEventListener('change',readFile,false);
    inputobj.type = 'file';
    inputobj.accept = 'image/*';
    inputobj.click();
}


//undo
function undo()
{
    if (step > 0) {
        step--;
        var img = new Image();
        img.src = userhistory[step];
        img.onload = function () { 
            ctx.clearRect(0, 0, width,height);
            ctx.drawImage(img, 0, 0); 
        }
    }
    else if(step==0){
        step--;
        ctx.clearRect(0, 0, width,height);
    }
}

//redo
function redo()
{
    if (step < userhistory.length-1) {
        step++;
        var img = new Image();
        img.src = userhistory[step];
        img.onload = function () {
            ctx.clearRect(0, 0, width,height); 
            ctx.drawImage(img, 0, 0); 
        }
    }
}

//reset function清空畫布
function reset()
{
    ctx.clearRect(0,0,width,height);
    userhistory=[];
    step=-1;
}

//輸入文字用到的function
function type()
{
    ctx.fillStyle='black';
    ctx.strokeStyle='black';
    ctx.lineWidth=5;
    ctx.save();
    ctx.beginPath();

    //開始寫字
    var size=document.getElementById("textsize").value;
    var style=document.getElementById("textstyle").value;
    ctx.font=size+"px"+" "+style;
    ctx.fillText(textcontent, parseInt(textbox.style.left), parseInt(textbox.style.top));

    context.restore();
    context.closePath();
}


function make_icon_colored(icon)
{
    icon.style.backgroundColor='blue';
}

function make_icon_trans(icon)
{
    icon.style.backgroundColor='transparent';
}

function choose_tool(choose)
{
    for(i=0;i<tool.length;i++)
    {
        if(i==choose)//選擇某樣工具
        {
            tool[i]=true;
            make_icon_colored(document.getElementById(tool_id[i]));
        }
        else//將其他工具取消
        {
            tool[i]=false;
            make_icon_trans(document.getElementById(tool_id[i]));
        }
    }
}


function take_pencil()//按下畫筆
{
    if(tool[0]==false)//選擇畫筆
    {
        choose_tool(0);
        document.getElementById('draw').style.cursor= "url('pencil.cur'), auto";
    }
    else//取消畫筆
    {
        tool[0]=false;
        make_icon_trans(document.getElementById('pencil'));//按下畫筆後icon背景變透明
        document.getElementById('draw').style.cursor='default';
    }
}

function take_eraser()//按下橡皮擦
{
    if(tool[1]==false)//選擇橡皮擦
    {
       choose_tool(1);
       document.getElementById('draw').style.cursor= "url('eraser.cur'), auto";
    }
    else//取消橡皮擦
    {
        tool[1]=false;
        make_icon_trans(document.getElementById('eraser'));//按下橡皮擦後icon背景變透明
        document.getElementById('draw').style.cursor='default';
    }
}

function take_text()//按下輸入文字
{
    if(tool[2]==false)
    {
       choose_tool(2);
       document.getElementById('draw').style.cursor= "url('text.cur'), auto";
    }
    else
    {
        tool[2]=false;
        make_icon_trans(document.getElementById('text'));
        document.getElementById('draw').style.cursor='default';
    }
}

function take_rec()//按下繪製長方形
{
    if(tool[3]==false)
    {
       choose_tool(3);
       document.getElementById('draw').style.cursor= "url('rectangle.cur'), auto";
    }
    else
    {
        tool[3]=false;
        make_icon_trans(document.getElementById('rectangle'));
        document.getElementById('draw').style.cursor='default';
    }
}

function take_cir()//按下繪製圓形
{
    if(tool[4]==false)
    {
       choose_tool(4);
       document.getElementById('draw').style.cursor= "url('circle.cur'), auto";
    }
    else
    {
        tool[4]=false;
        make_icon_trans(document.getElementById('circle'));
        document.getElementById('draw').style.cursor='default';
    }
}

function take_tri()//按下繪製三角形
{
    if(tool[5]==false)
    {
       choose_tool(5);
       document.getElementById('draw').style.cursor= "url('triangle.cur'), auto";
    }
    else
    {
        tool[5]=false;
        make_icon_trans(document.getElementById('triangle'));
        document.getElementById('draw').style.cursor='default';
    }
}

//按下滑鼠
canvas.addEventListener('mousedown',function(e){
    is_mouse_active=true;//滑鼠觸發

    if(tool[2]!=true)textbox.style['z-index'] = 1;//選擇其他工具時將文字框隱藏

    if(tool[0]==true)//拿畫筆
    {
        start_x=e.offsetX;
        start_y=e.offsetY;
        ctx.lineCap = 'round';
        ctx.lineJoin = 'round';

        ctx.lineWidth=document.getElementById("pencil_width").value;//選畫筆大小
        ctx.strokeStyle=document.getElementById('color-label').style.backgroundColor;//選顏色
    }

    else if(tool[1]==true)//拿橡皮擦
    {
        start_x=e.offsetX;
        start_y=e.offsetY;
        ctx.save();
        ctx.beginPath();
        ctx.arc(start_x,start_y,10,0,2*Math.PI);//第三個數字調整橡皮擦寬度
        ctx.clip();
        ctx.clearRect(0,0,width,height);
        ctx.restore();
    }

    else if(tool[2]==true)//按下寫字icon
    {
        start_x=e.offsetX;
        start_y=e.offsetY;

        if (textflag) {
            textcontent = textbox.value;
            textflag = false;
            textbox.style['z-index'] = 1;
            textbox.value = "";
            type();
        } else if (!textflag) {
            textflag = true
            textbox.style.left = start_x + 'px';
            textbox.style.top = start_y + 'px';
            textbox.style['z-index'] = 6;
        }
    }

    else if(tool[3]==true||tool[4]==true||tool[5]==true)//按下長方形、圓形、三角形icon
    {
        start_x=e.offsetX;
        start_y=e.offsetY;
        ctx.lineCap = 'round';
        ctx.lineJoin = 'round';

        ctx.lineWidth=document.getElementById("pencil_width").value;//選畫筆大小
        ctx.strokeStyle=document.getElementById('color-label').style.backgroundColor;//選顏色
    }
})

//移動滑鼠
canvas.addEventListener('mousemove',function(e)
{
    if(is_mouse_active==false)return;//滑鼠沒觸發就不算移動

    if(tool[0]==true)//拿畫筆
    {
        end_x=e.offsetX;
        end_y=e.offsetY;
        //繪圖
        ctx.beginPath();
        ctx.moveTo(start_x,start_y);
        ctx.lineTo(end_x,end_y);
        ctx.stroke();

        start_x=end_x;
        start_y=end_y;
    }

    else if(tool[1]==true)//拿橡皮擦
    {
        end_x=e.offsetX;
        end_y=e.offsetY;

        var asin = 10*Math.sin(Math.atan((end_y-start_y)/(end_x-start_x)));
        var acos = 10*Math.cos(Math.atan((end_y-start_y)/(end_x-start_x)));
        var x3 = start_x+asin;
        var y3 = start_y-acos;
        var x4 = start_x-asin;
        var y4 = start_y+acos;
        var x5 = end_x+asin;
        var y5 = end_y-acos;
        var x6 = end_x-asin;
        var y6 = end_y+acos;


        ctx.save();
        ctx.beginPath();
        ctx.arc(end_x,end_y,10,0,2*Math.PI);
        ctx.clip();
        ctx.clearRect(0,0,width,height);
        ctx.restore();

        ctx.save();
        ctx.beginPath();
        ctx.moveTo(x3,y3);
        ctx.lineTo(x5,y5);
        ctx.lineTo(x6,y6);
        ctx.lineTo(x4,y4);
        ctx.closePath();
        ctx.clip()
        ctx.clearRect(0,0,width,height);
        ctx.restore();

        start_x=end_x;
        start_y=end_y;
    }

    else if(tool[3]==true)//畫長方形
    {
        end_x=e.offsetX;
        end_y=e.offsetY;
        ctx.clearRect(0,0,width,height);
        loadImage();
        ctx.beginPath();
        ctx.strokeRect(start_x,start_y,end_x-start_x,end_y-start_y);
    }

    else if(tool[4]==true)//畫圓形
    {
        end_x=e.offsetX;
        end_y=e.offsetY;
        ctx.clearRect(0,0,width,height);
        loadImage();
        ctx.beginPath();
        var rx = (end_x-start_x)/2;
        var ry = (end_y-start_y)/2;
        var r = Math.sqrt(rx*rx+ry*ry);
        ctx.arc(rx+start_x,ry+start_y,r,0,Math.PI*2);
        ctx.stroke();
    }

    else if(tool[5]==true)//畫三角形
    {
        end_x=e.offsetX;
        end_y=e.offsetY;
        ctx.clearRect(0,0,width,height);
        loadImage();
        ctx.beginPath();
        ctx.moveTo(start_x,start_y);
        ctx.lineTo(end_x,end_y);
        ctx.lineTo(start_x-(end_x-start_x),end_y);
        ctx.closePath();
        ctx.stroke();
    }
})

//放開滑鼠
canvas.addEventListener('mouseup',function(e){
    is_mouse_active=false;
    url = canvas.toDataURL();
    
    
    //undo redo
    step++;
    if(step<userhistory.length)userhistory.length=step;
    userhistory.push(url);


    loadImage();
})

canvas.addEventListener('mouseout',function(){
    is_mouse_active=false;
})
